(function(factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else {
        factory(window.jQuery);
    }
}(function($) {

    $.fn.numeric = function(config, callback) {
        if (typeof config === 'boolean') {
            config = {
                decimal: config,
                negative: true,
                decimalPlaces: -1
            };
        }
        config = config || {};
        // if config.negative undefined, set to true (default is to allow negative numbers)
        if (typeof config.negative == "undefined") {
            config.negative = true;
        }
        // set decimal point
        var decimal = (config.decimal === false) ? "" : config.decimal || ".";
        // allow negatives
        var negative = (config.negative === true) ? true : false;
        // set decimal places
        var decimalPlaces = (typeof config.decimalPlaces == "undefined") ? -1 : config.decimalPlaces;
        // callback function
        callback = (typeof(callback) == "function" ? callback : function() {});
        // set data and methods
        return this.data("numeric.decimal", decimal).data("numeric.negative", negative).data("numeric.callback", callback).data("numeric.decimalPlaces", decimalPlaces).keypress($.fn.numeric.keypress).keyup($.fn.numeric.keyup).blur($.fn.numeric.blur);
    };

    $.fn.numeric.keypress = function(e) {
        // get decimal character and determine if negatives are allowed
        var decimal = $.data(this, "numeric.decimal");
        var negative = $.data(this, "numeric.negative");
        var decimalPlaces = $.data(this, "numeric.decimalPlaces");
        // get the key that was pressed
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        // allow enter/return key (only when in an input box)
        if (key == 13 && this.nodeName.toLowerCase() == "input") {
            return true;
        } else if (key == 13) {
            return false;
        }
        //dont allow #, $, %
        else if (key == 35 || key == 36 || key == 37) {
            return false;
        }
        var allow = false;
        // allow Ctrl+A
        if ((e.ctrlKey && key == 97 /* firefox */ ) || (e.ctrlKey && key == 65) /* opera */ ) {
            return true;
        }
        // allow Ctrl+X (cut)
        if ((e.ctrlKey && key == 120 /* firefox */ ) || (e.ctrlKey && key == 88) /* opera */ ) {
            return true;
        }
        // allow Ctrl+C (copy)
        if ((e.ctrlKey && key == 99 /* firefox */ ) || (e.ctrlKey && key == 67) /* opera */ ) {
            return true;
        }
        // allow Ctrl+Z (undo)
        if ((e.ctrlKey && key == 122 /* firefox */ ) || (e.ctrlKey && key == 90) /* opera */ ) {
            return true;
        }
        // allow or deny Ctrl+V (paste), Shift+Ins
        if ((e.ctrlKey && key == 118 /* firefox */ ) || (e.ctrlKey && key == 86) /* opera */ ||
            (e.shiftKey && key == 45)) {
            return true;
        }
        // if a number was not pressed
        if (key < 48 || key > 57) {
            var value = $(this).val();
            /* '-' only allowed at start and if negative numbers allowed */
            if ($.inArray('-', value.split('')) !== 0 && negative && key == 45 && (value.length === 0 || parseInt($.fn.getSelectionStart(this), 10) === 0)) {
                return true;
            }
            /* only one decimal separator allowed */
            if (decimal && key == decimal.charCodeAt(0) && $.inArray(decimal, value.split('')) != -1) {
                allow = false;
            }
            // check for other keys that have special purposes
            if (
                key != 8 /* backspace */ &&
                key != 9 /* tab */ &&
                key != 13 /* enter */ &&
                key != 35 /* end */ &&
                key != 36 /* home */ &&
                key != 37 /* left */ &&
                key != 39 /* right */ &&
                key != 46 /* del */
            ) {
                allow = false;
            } else {
                // for detecting special keys (listed above)
                // IE does not support 'charCode' and ignores them in keypress anyway
                if (typeof e.charCode != "undefined") {
                    // special keys have 'keyCode' and 'which' the same (e.g. backspace)
                    if (e.keyCode == e.which && e.which !== 0) {
                        allow = true;
                        // . and delete share the same code, don't allow . (will be set to true later if it is the decimal point)
                        if (e.which == 46) {
                            allow = false;
                        }
                    }
                    // or keyCode != 0 and 'charCode'/'which' = 0
                    else if (e.keyCode !== 0 && e.charCode === 0 && e.which === 0) {
                        allow = true;
                    }
                }
            }
            // if key pressed is the decimal and it is not already in the field
            if (decimal && key == decimal.charCodeAt(0)) {
                if ($.inArray(decimal, value.split('')) == -1) {
                    allow = true;
                } else {
                    allow = false;
                }
            }
        } else {
            allow = true;
            // remove extra decimal places
            if (decimal && decimalPlaces > 0) {
                var selectionStart = $.fn.getSelectionStart(this);
                var selectionEnd = $.fn.getSelectionEnd(this);
                var dot = $.inArray(decimal, $(this).val().split(''));
                if (selectionStart === selectionEnd && dot >= 0 && selectionStart > dot && $(this).val().length > dot + decimalPlaces) {
                    allow = false;
                }
            }

        }
        return allow;
    };

    $.fn.numeric.keyup = function(e) {
        var val = $(this).val();

        if (val && val.length > 0) {
            // get carat (cursor) position
            var carat = $.fn.getSelectionStart(this);
            var selectionEnd = $.fn.getSelectionEnd(this);
            // get decimal character and determine if negatives are allowed
            var decimal = $.data(this, "numeric.decimal");
            var negative = $.data(this, "numeric.negative");
            var decimalPlaces = $.data(this, "numeric.decimalPlaces");

            // prepend a 0 if necessary
            if (decimal !== "" && decimal !== null) {
                // find decimal point
                var dot = $.inArray(decimal, val.split(''));
                // if dot at start, add 0 before
                if (dot === 0) {
                    this.value = "0" + val;
                    carat++;
                    selectionEnd++;
                }
                // if dot at position 1, check if there is a - symbol before it
                if (dot == 1 && val.charAt(0) == "-") {
                    this.value = "-0" + val.substring(1);
                    carat++;
                    selectionEnd++;
                }
                val = this.value;
            }



            // if pasted in, only allow the following characters
            var validChars = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, '-', decimal];
            // get length of the value (to loop through)
            var length = val.length;
            // loop backwards (to prevent going out of bounds)
            for (var i = length - 1; i >= 0; i--) {
                var ch = val.charAt(i);
                // remove '-' if it is in the wrong place
                if (i !== 0 && ch == "-") {
                    val = val.substring(0, i) + val.substring(i + 1);
                }
                // remove character if it is at the start, a '-' and negatives aren't allowed
                else if (i === 0 && !negative && ch == "-") {
                    val = val.substring(1);
                }
                var validChar = false;
                // loop through validChars
                for (var j = 0; j < validChars.length; j++) {
                    // if it is valid, break out the loop
                    if (ch == validChars[j]) {
                        validChar = true;
                        break;
                    }
                }
                // if not a valid character, or a space, remove
                if (!validChar || ch == " ") {
                    val = val.substring(0, i) + val.substring(i + 1);
                }
            }
            // remove extra decimal characters
            var firstDecimal = $.inArray(decimal, val.split(''));
            if (firstDecimal > 0) {
                for (var k = length - 1; k > firstDecimal; k--) {
                    var chch = val.charAt(k);
                    // remove decimal character
                    if (chch == decimal) {
                        val = val.substring(0, k) + val.substring(k + 1);
                    }
                }
            }

            // remove extra decimal places
            if (decimal && decimalPlaces > 0) {
                var dot = $.inArray(decimal, val.split(''));
                if (dot >= 0) {
                    val = val.substring(0, dot + decimalPlaces + 1);
                    selectionEnd = Math.min(val.length, selectionEnd);
                }
            }
            // set the value and prevent the cursor moving to the end
            this.value = val;
            $.fn.setSelection(this, [carat, selectionEnd]);
        }
    };

    $.fn.numeric.blur = function() {
        var decimal = $.data(this, "numeric.decimal");
        var callback = $.data(this, "numeric.callback");
        var negative = $.data(this, "numeric.negative");
        var val = this.value;
        if (val !== "") {
            var re = new RegExp("^" + (negative ? "-?" : "") + "\\d+$|^" + (negative ? "-?" : "") + "\\d*" + decimal + "\\d+$");
            if (!re.exec(val)) {
                callback.apply(this);
            }
        }
    };

    $.fn.removeNumeric = function() {
        return this.data("numeric.decimal", null).data("numeric.negative", null).data("numeric.callback", null).data("numeric.decimalPlaces", null).unbind("keypress", $.fn.numeric.keypress).unbind("keyup", $.fn.numeric.keyup).unbind("blur", $.fn.numeric.blur);
    };

    // Based on code from http://javascript.nwbox.com/cursor_position/ (Diego Perini <dperini@nwbox.com>)
    $.fn.getSelectionStart = function(o) {
        if (o.type === "number") {
            return undefined;
        } else if (o.createTextRange && document.selection) {
            var r = document.selection.createRange().duplicate();
            r.moveEnd('character', o.value.length);
            if (r.text == '') return o.value.length;

            return Math.max(0, o.value.lastIndexOf(r.text));
        } else {
            try {
                return o.selectionStart;
            } catch (e) {
                return 0;
            }
        }
    };

    // Based on code from http://javascript.nwbox.com/cursor_position/ (Diego Perini <dperini@nwbox.com>)
    $.fn.getSelectionEnd = function(o) {
        if (o.type === "number") {
            return undefined;
        } else if (o.createTextRange && document.selection) {
            var r = document.selection.createRange().duplicate()
            r.moveStart('character', -o.value.length)
            return r.text.length
        } else return o.selectionEnd
    }

    // set the selection, o is the object (input), p is the position ([start, end] or just start)
    $.fn.setSelection = function(o, p) {
        // if p is number, start and end are the same
        if (typeof p == "number") {
            p = [p, p];
        }
        // only set if p is an array of length 2
        if (p && p.constructor == Array && p.length == 2) {
            if (o.type === "number") {
                o.focus();
            } else if (o.createTextRange) {
                var r = o.createTextRange();
                r.collapse(true);
                r.moveStart('character', p[0]);
                r.moveEnd('character', p[1] - p[0]);
                r.select();
            } else {
                o.focus();
                try {
                    if (o.setSelectionRange) {
                        o.setSelectionRange(p[0], p[1]);
                    }
                } catch (e) {}
            }
        }
    };

}));

(function() {
    var $ACTIVE_CELL,
        SPIN_EVENT;

    function isChecked($el) {
        return $el.is(':checked');
    }

    function isActive($el) {
        return $el.hasClass('active');
    }

    function isDefined(el) {
        return (el == undefined);
    }

    function isEmpty(el) {
        return (el == "");
    }

    function addNewSelection($el) {
        if (!$el.hasClass('active')) {
            $el.addClass('active');
        }
    }

    function removePrevSelection($el) {
        $el.each(function() {
            var $this = $(this);

            if ($this.hasClass('active')) {
                $this.removeClass('active');
            }
        });
    }

    function resetPosition($monitor) {
        var $lastMonitor, $monitorWidget;

        if (isActive($monitor.embedPosition)) {
            $lastMonitor = $monitor.embedStretch;
        } else if (isActive($monitor.embedStretch)) {
            $lastMonitor = $monitor.embedPosition;
        }

        $monitorWidget = $lastMonitor.find('.webform-monitor-widget');

        $monitorWidget.find('.cell').each(function() {
            var $this = $(this);

            if (isActive($this)) {
                $this.removeClass('active');
            }
        });

        $('.js-input').each(function() {
            var $input = $(this).closest('.number-spinner-holder');
            $input.addClass('disabled');
        });
    }

    function positionPicker(callback) {
        var $parent, $hiddenField;

        $parent = $('.webform-position-grid');
        $hiddenField = $('#id_embed_position');

        $parent.find('.cell').click(function() {
            var $this = $(this),
                $id = $this.attr('id'),
                $pos = $this.data('position');

            removePrevSelection($this.parent().find('.active'));
            addNewSelection($this);
            $hiddenField.val($pos);

            callback && callback($this)
        });
    }

    function showCurrentMonitor($el, $monitor) {
        var $this = $el,
            $checkbox = $('#id_embed_position');

        if (isChecked($this)) {
            var $embedPosition = $monitor.embedPosition,
                $embedStretch = $monitor.embedStretch;

            removePrevSelection($embedPosition);
            addNewSelection($embedStretch);

        } else if (!isChecked($this)) {
            var $embedPosition = $monitor.embedPosition,
                $embedStretch = $monitor.embedStretch;

            removePrevSelection($embedStretch);
            addNewSelection($embedPosition);
        }
    }

    function activateToggleMonitorsEvent($el, $monitor) {
        $el.on('click', function() {
            var $this = $(this);

            showCurrentMonitor($this, $monitor);
            resetPosition($monitor);
        });

    }

    function getOffsetLimit(type, position) {
        var limitMapPosition = {},
            limitMapStretch = {},
            bound = 999;
        result = {};

        limitMapPosition = {
            10: {
                x: [0, bound],
                y: [0, bound]
            },
            20: {
                x: [-bound, bound],
                y: [0, bound]
            },
            30: {
                x: [-bound, 0],
                y: [0, bound]
            },
            40: {
                x: [0, bound],
                y: [-bound, bound]
            },
            50: {
                x: [-bound, bound],
                y: [-bound, bound]
            },
            60: {
                x: [-bound, 0],
                y: [-bound, bound]
            },
            70: {
                x: [0, bound],
                y: [-bound, 0]
            },
            80: {
                x: [-bound, bound],
                y: [-bound, 0]
            },
            90: {
                x: [-bound, 0],
                y: [-bound, 0]
            }
        };

        limitMapStretch = {
            20: {
                x: [-bound, bound],
                y: [0, 0]
            },
            40: {
                x: [0, 0],
                y: [-bound, bound]
            },
            60: {
                x: [0, 0],
                y: [-bound, bound]
            },
            80: {
                x: [-bound, bound],
                y: [0, 0]
            }
        };

        result = (type == 'cell') ? limitMapPosition[position] : limitMapStretch[position];

        return result;
    }

    function setOffsetControls($parent, type, position) {
        var $xAxisParent = $parent.find('.js-xAxis'),
            $yAxisParent = $parent.find('.js-yAxis'),
            $xAxisInput = $xAxisParent.find('.js-xAxisInput'),
            $yAxisInput = $yAxisParent.find('.js-yAxisInput'),
            limit = {};

        limit = {
            xMin: getOffsetLimit(type, position).x[0],
            xMax: getOffsetLimit(type, position).x[1],
            yMin: getOffsetLimit(type, position).y[0],
            yMax: getOffsetLimit(type, position).y[1]
        };

        $xAxisInput.attr('min', limit.xMin);
        $xAxisInput.attr('max', limit.xMax);

        $yAxisInput.attr('min', limit.yMin);
        $yAxisInput.attr('max', limit.yMax);

    }

    function inputLimitCheck($input) {
        $input.on('keyup', function(e) {
            var $this = $(this),
                val = parseInt($this.val()),
                inputLimit = {
                    min: parseInt($this.attr('min')),
                    max: parseInt($this.attr('max'))
                };

            var allow = false;

            if (val > inputLimit.max) {
                $this.val(inputLimit.max);
            } else if (val < inputLimit.min) {
                $this.val(inputLimit.min);
            } else {
                allow = true;
            }

            if (allow) {
                var $storage = $($input.data('target'));

                $storage.val($input.val());

                var $button = {
                    down: $this.closest('.number-spinner-holder').find('.js-spinner-control-minus'),
                    up: $this.closest('.number-spinner-holder').find('.js-spinner-control-plus')
                };

                if (val == inputLimit.min) {
                    $button.down.addClass('disabled');
                    $button.up.removeClass('disabled');
                }

                if (val == inputLimit.max) {
                    $button.up.addClass('disabled');
                    $button.down.removeClass('disabled');
                }

                if (val > inputLimit.min && val < inputLimit.max) {
                    $button.up.removeClass('disabled');
                    $button.down.removeClass('disabled');
                }
            }

        });

    }

    function saveOffsets($parentWrapper, hOffset, vOffset) {
        var $horizontalOffsetField = $('#id_embed_horizontal_offset'),
            $verticalOffsetField = $('#id_embed_vertical_offset'),
            $xAxisInput = $parentWrapper.find('.js-xAxisInput'),
            $yAxisInput = $parentWrapper.find('.js-yAxisInput');

        $horizontalOffsetField.val(hOffset);
        $verticalOffsetField.val(vOffset);
        $xAxisInput.val(hOffset);
        $yAxisInput.val(vOffset);
    }

    function activateMonitorActions($cell, hasTrigger) {
        var $this = $cell,
            cellPosition = $this.data('position'),
            cellType = ($this.hasClass('hasStretch')) ? 'stretch' : 'cell',
            $parentWrapper = $this.closest('.js-webform-position-grid'),
            $xAxisInput = $parentWrapper.find('.js-xAxisInput'),
            $yAxisInput = $parentWrapper.find('.js-yAxisInput'),
            $control = {},

            $control = {
                xAxis: {
                    up: $xAxisInput.closest('.number-spinner-holder').find('.js-spinner-control-plus'),
                    down: $xAxisInput.closest('.number-spinner-holder').find('.js-spinner-control-minus')
                },
                yAxis: {
                    up: $yAxisInput.closest('.number-spinner-holder').find('.js-spinner-control-plus'),
                    down: $yAxisInput.closest('.number-spinner-holder').find('.js-spinner-control-minus')
                }
            };

        $('.js-input').each(function() {
            var $input = $(this).closest('.number-spinner-holder');
            $input.removeClass('disabled');
        });

        if (cellType == 'stretch') {
            if (cellPosition == "20" || cellPosition == "80") {
                $yAxisInput.closest('.number-spinner-holder').addClass('disabled');
            } else {
                $xAxisInput.closest('.number-spinner-holder').addClass('disabled');
            }
        }

        if (hasTrigger) {
            /*$xAxisInput.val('0');
            $yAxisInput.val('0');*/
            saveOffsets($parentWrapper, 0, 0);
        }

        setOffsetControls($parentWrapper, cellType, cellPosition);


        $xAxisInput.numeric();
        $yAxisInput.numeric();

        inputLimitCheck($xAxisInput);
        inputLimitCheck($yAxisInput);

        if ($xAxisInput.attr('min') == 0) {
            $control.xAxis.down.addClass('disabled');
        } else if ($xAxisInput.attr('min') != 0) {
            $control.xAxis.down.removeClass('disabled');
        }

        if ($xAxisInput.attr('max') == 0) {
            $control.xAxis.up.addClass('disabled');
        } else if ($xAxisInput.attr('max') != 0) {
            $control.xAxis.up.removeClass('disabled');
        }

        if ($yAxisInput.attr('min') == 0) {
            $control.yAxis.down.addClass('disabled');
        } else if ($yAxisInput.attr('min') != 0) {
            $control.yAxis.down.removeClass('disabled');
        }

        if ($yAxisInput.attr('max') == 0) {
            $control.yAxis.up.addClass('disabled');
        } else if ($yAxisInput.attr('max') != 0) {
            $control.yAxis.up.removeClass('disabled');
        }
    }

    function setActiveCell($cell, hasTrigger) {
        var $this = $cell,
            $id = $this.attr('id'),
            $pos = $this.data('position'),
            $inputPos = $('#id_embed_position');

        removePrevSelection($this.parent().find('.active'));

        addNewSelection($this);
        //$hiddenField.val($pos);
        $inputPos.val($pos);

        $ACTIVE_CELL = $this;

        activateMonitorActions($this, hasTrigger);

    }

    function setInitialState($storage) {
        if ($storage.hOffset.val() == "" && $storage.vOffset.val() == "") {
            $('.js-input').each(function() {
                var $inputWrapper = $(this).closest('.number-spinner-holder');
                $inputWrapper.addClass('disabled');
            });
        } else {
            var $activeGrid, 
                $activeCell, 
                activeCellTarget;

            $('.js-xAxisInput').val($storage.hOffset.val());
            $('.js-yAxisInput').val($storage.vOffset.val());

            if (isChecked($storage.cellType)) {
                $activeGrid = $storage.monitor.embedStretch;
            } else {
                $activeGrid = $storage.monitor.embedPosition;
            }

            activeCellTarget = $storage.position.val();
            $activeCell = $activeGrid.find('.cell.embed-position-' + activeCellTarget);

            setActiveCell($activeCell, false);
        }
    }

    function numberSpin($button, isOnlyClick) {
        var $input = $button.closest('.number-spinner-holder').find('input'),
            $storage = $($input.data('target')),
            $oppositeButton = null;

        $button
            .closest('.number-spinner-holder')
            .find('.js-spinner-control')
            .prop("disabled", false);

        if ($button.hasClass('js-spinner-control-plus')) {
            $oppositeButton = $button.closest('.number-spinner-holder').find('.js-spinner-control-minus');
        } else if ($button.hasClass('js-spinner-control-minus')) {
            $oppositeButton = $button.closest('.number-spinner-holder').find('.js-spinner-control-plus');
        }

        function increase() {
            if ($input.attr('max') == undefined || parseInt($input.val()) < parseInt($input.attr('max'))) {
                var result = parseInt($input.val()) + 1
                $input.val(result);
                $storage.val(result);
                $button.removeClass('disabled');

                if (parseInt($input.val()) > parseInt($input.attr('min'))) {
                    $oppositeButton.removeClass('disabled');
                }

                if (parseInt($input.val()) == parseInt($input.attr('min')) || parseInt($input.val()) == parseInt($input.attr('max'))) {
                    $button.addClass('disabled');
                }

            } else {
                $button.addClass('disabled');
                clearInterval(SPIN_EVENT);
            }
        }

        function decrease() {
            if ($input.attr('min') == undefined || parseInt($input.val()) > parseInt($input.attr('min'))) {
                var result = parseInt($input.val()) - 1
                $input.val(result);
                $storage.val(result);
                $button.removeClass('disabled');

                if (parseInt($input.val()) < parseInt($input.attr('max'))) {
                    $oppositeButton.removeClass('disabled');
                }

                if (parseInt($input.val()) == parseInt($input.attr('min')) || parseInt($input.val()) == parseInt($input.attr('max'))) {
                    $button.addClass('disabled');
                }

            } else {
                $button.addClass('disabled');
                clearInterval(SPIN_EVENT);
            }
        }
        if ($button.attr('data-dir') == 'plus') {
            if(isOnlyClick) {
                increase();
                clearInterval(SPIN_EVENT);              
            } else {
                SPIN_EVENT = setInterval(function() {
                    increase();
                }, 130)
                
            }
        } else if ($button.attr('data-dir') == 'minus') {
            if(isOnlyClick) {
                decrease();
                clearInterval(SPIN_EVENT);
            } else {
                SPIN_EVENT = setInterval(function() {
                    decrease();
                }, 130); 
            }            
        }     
    }

    $(function() {        
        var $embedMonitorToggler = $('#id_embed_stretch'),
            $monitor = {},
            $storage = {};

        $monitor = {
            embedPosition: $('#webform-embed-position'),
            embedStretch: $('#webform-embed-stretch')
        };

        $storage = {
            grid: $('.webform-position-grid'),
            hOffset: $('#id_embed_horizontal_offset'),
            vOffset: $('#id_embed_vertical_offset'),
            position: $('#id_embed_position'),
            cellType: $('#id_embed_stretch'),
            monitor: $monitor
        };

        setInitialState($storage);

        showCurrentMonitor($embedMonitorToggler, $monitor);
        activateToggleMonitorsEvent($embedMonitorToggler, $monitor);

        $storage.grid.find('.cell').click(function() {
            var $this = $(this);
            setActiveCell($this, true);
        });

        // Range offset component event
        $(".number-spinner-holder .js-spinner-control").on({
            mousedown: function() {
                numberSpin($(this), false);    
            },
            mouseup: function() {
                clearInterval(SPIN_EVENT);
            },
            mouseleave: function() {
                clearInterval(SPIN_EVENT);
            },
            click: function() {
                numberSpin($(this), true);
            }
        });
    });

})();